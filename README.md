Project to show how a Webdriver Factory could be set up using Java.

Uses global test.properties file to set browser and GRID parameters, which are loaded and set to static variables using the TestConfiguration class.

The TestDriverFactory controls building the Webdriver instance for the tests, using the DriverFactory, which is either local or remote (if the remote property has been set to true)

The local/remote configurations are controlled by the respective LocalDriver/RemoteDriver configurations classes.

Page Objects are used in the tests.

Junit suites are set up which can be configured as required and run using Maven profiles. A couple have been set up by way of example. 

Logging is taken care of using log4j

A .bat file is provided which overwrites values in test.properties in accordance with the parameters passed in.  These can be
passed in via the command line, or using a CI server.  For example, in Jenkins, set up parameters and use these to run a pre build
command e.g. 
"loadTestProperties.bat %REMOTE% %BROWSER% %PLATFORM% %BROWSER_VERSION% %SELENIUM_HUB_URL% %SELENIUM_HUB_PORT% %APPLICATION URL%"
You could have different Jenkins jobs for each browser, for example. 

If you needed to filter the tests, you can use the maven profiles.  e.g. use the goal "clean test -P<profile_id>"