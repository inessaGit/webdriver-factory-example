package jnhamilton.selenium.webdriverFactory.configurations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class used to construct a remote driver to send to GRID hub
 */

public class RemoteDriverConfiguration {

    public String browser;
    public String platform;
    public String browserVersion;
    public String seleniumHubUrl;
    public int seleniumHubPort;

    private static final Logger LOG = LogManager.getLogger(RemoteDriverConfiguration.class);

    public RemoteDriverConfiguration(String browser, String platform, String browserVersion, String seleniumHubUrl,
                                          int seleniumHubPort){
        this.browser=browser;
        this.platform=platform;
        this.browserVersion=browserVersion;
        this.seleniumHubUrl=seleniumHubUrl;
        this.seleniumHubPort=seleniumHubPort;

        LOG.info("The remote driver properties loaded were -  " +
                "browser: " + browser +
                "platform: " +  platform +
                "browser version: " + browserVersion +
                "seleniumHubUrl: " + seleniumHubUrl +
                "seleniumHubPort: " + seleniumHubPort);
    }
}
