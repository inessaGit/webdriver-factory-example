package jnhamilton.selenium.webdriverFactory.configurations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class used to construct a local driver
 * For a local driver we just need the browser
 */

public class LocalDriverConfiguration {

    public String browser;
    private static final Logger LOG = LogManager.getLogger(RemoteDriverConfiguration.class);

    public LocalDriverConfiguration(String browser){

        this.browser=browser;

        LOG.info("The local driver properties loaded were - " +
                "browser: " + browser);
    }
}
