package jnhamilton.selenium.webdriverFactory;

import jnhamilton.selenium.webdriverFactory.configurations.LocalDriverConfiguration;
import jnhamilton.selenium.webdriverFactory.configurations.RemoteDriverConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Webdriver factory class that configures and returns either a local or remote driver instance
 * as required
 * At the moment, Marionette is kept separately from Firefox to allow the choice of where to run the tests
 * This is because Marionette doesn't yet support all the functionality.
 * For Firefox v46 and above, you need to use Marionette
 */

public class DriverFactory {

        private DesiredCapabilities _capabilities;
        private WebDriver _driver;
        private static final Logger LOG = LogManager.getLogger(DriverFactory.class);

    /**
     * Creates a local driver, as it takes the LocalDriver Configuration object
     * @param configuration the local driver configuration
     * @return instance of the local driver as configured
     */
    public WebDriver createLocalDriver(LocalDriverConfiguration configuration){

        switch(configuration.browser){

            case "chrome":
                createChromeDriver();
                break;

            case "internet explorer":
                createInternetExplorerDriver();
                break;

            case "firefox":
                createFireFoxDriver();
                break;

            case "edge":
                createEdgeDriver();
                break;

            //defaults to chrome
            default:
                createChromeDriver();
                break;
        }

    return _driver;
    }

    /**
     * Creates a remote driver, as it takes the RemoteDriver configuration object
     * @param configuration the remote driver configuration
     * @return instance of the remote driver as configured
     */
    public WebDriver createRemoteDriver(RemoteDriverConfiguration configuration)  {

        String remoteServer = buildRemoteServer(configuration.seleniumHubUrl, configuration.seleniumHubPort);

            switch (configuration.browser) {

                case "firefox":
                    _capabilities = DesiredCapabilities.firefox();
                    break;

                case "chrome":
                    _capabilities = DesiredCapabilities.chrome();
                    break;

                case "internet explorer":
                    _capabilities = DesiredCapabilities.internetExplorer();
                    break;

            }

            setCapabilities(configuration.platform, configuration.browserVersion);

        try {
            _driver = new RemoteWebDriver(new URL(remoteServer),_capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return _driver;
        }

    private void setCapabilities(String platform, String browserVersion){
        _capabilities.setCapability(CapabilityType.PLATFORM, platform);
        _capabilities.setCapability(CapabilityType.VERSION, browserVersion);
    }

    /**
     * Sets the relevant driver properties for the local driver instance
     * @param propKey  the driver property to set
     * @param relativeToUserPath the relative location of the exe file
     */
    private static void setDriverPropertyIfRequired(String propKey, String relativeToUserPath){

        if(!System.getProperties().containsKey(propKey)) {

            String currentDir = System.getProperty("user.dir");
            String driverLocation = currentDir + relativeToUserPath;
            File driverExecutable = new File(driverLocation);

            try {
                if (driverExecutable.exists()) {
                    System.setProperty(propKey, driverLocation);
                }
            } catch (Exception e) {
                LOG.error("The driver does not exist at that location: " + driverLocation);
            }
        }
    }


    /**
     * Build a Uri for your GRID Hub instance
     * @param remoteServer  The hostname or IP address of your GRID instance, include the http://
     * @param remoteServerPort Port of your GRID Hub instance
     * @return the URL as a string
     */
    public static String buildRemoteServer(String remoteServer, int remoteServerPort){
        return String.format("%s/%d/wd/hub", remoteServer, remoteServerPort);
    }

    private WebDriver createChromeDriver(){
        //Chrome driver requires the webdriver property to be set
        //Sets property if not supplied
        setDriverPropertyIfRequired("webdriver.chrome.driver","/tools/chromedriverv2.29/chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-plugins");
        options.addArguments("disable-extensions");
        options.addArguments("test-type");

        _driver = new ChromeDriver(options);
        return _driver;
    }

    private WebDriver createFireFoxDriver(){
        //Firefox driver requires the webdriver property to be set
        setDriverPropertyIfRequired("webdriver.gecko.driver", "/tools/geckodriver-v0.16.0/geckodriver.exe");

        _driver = new FirefoxDriver();
        return _driver;
    }


    private WebDriver createInternetExplorerDriver(){
        //IE driver requires the webdriver property to be set
        //Sets property if not supplied
        setDriverPropertyIfRequired("webdriver.ie.driver", "/tools/iedriver32bit/IEDriverServer.exe");

        _driver = new InternetExplorerDriver();
        return _driver;
    }

    private WebDriver createEdgeDriver(){
        //Edge Driver requires you install the Microsoft Webdriver Server, then set the system property
        setDriverPropertyIfRequired("webdriver.edge.driver","/tools/edgedriver14393/MicrosoftWebDriver.exe");

        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.setPageLoadStrategy("eager");
        _driver = new EdgeDriver(edgeOptions);
        return _driver;
    }
}
