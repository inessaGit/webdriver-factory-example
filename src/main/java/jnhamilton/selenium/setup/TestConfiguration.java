package jnhamilton.selenium.setup;

import java.io.IOException;

import static jnhamilton.selenium.setup.LoadProperties.*;

/**
 * Reads the values from the test.properties file and stores them in static variables that can be used by the Driver
 * Factory and in the tests as required.
 */

public class TestConfiguration {

    public static boolean remote;
    public static String browser;
    public static String platform;
    public static String browserVersion;
    public static String seleniumHubUrl;
    public static int seleniumHubPort;
    public static String applicationUrl;


    static {
        try {
            remote = Boolean.parseBoolean(getPropertyValue("test.properties", "Remote"));
            browser = getPropertyValue("test.properties", "Browser");
            platform = getPropertyValue("test.properties", "Platform");
            browserVersion = getPropertyValue("test.properties", "BrowserVersion");
            seleniumHubUrl = getPropertyValue("test.properties","SeleniumHubUrl");
            seleniumHubPort = Integer.parseInt(getPropertyValue("test.properties","SeleniumHubPort"));
            applicationUrl = getPropertyValue("test.properties","ApplicationUrl");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
