package jnhamilton.selenium.setup;

import jnhamilton.selenium.webdriverFactory.DriverFactory;
import jnhamilton.selenium.webdriverFactory.configurations.LocalDriverConfiguration;
import jnhamilton.selenium.webdriverFactory.configurations.RemoteDriverConfiguration;
import org.openqa.selenium.WebDriver;

/**
 * Factory class that compiles a remote or local driver instance dependent on the state of the remote property as
 * set in test.properties
 */

public class TestDriverFactory {

    public WebDriver createDriver()  {
        if(TestConfiguration.remote){
            return new DriverFactory().createRemoteDriver(
                    new RemoteDriverConfiguration(
                            TestConfiguration.browser,
                            TestConfiguration.platform,
                            TestConfiguration.browserVersion,
                            TestConfiguration.seleniumHubUrl,
                            TestConfiguration.seleniumHubPort)
            );
        }

        return new DriverFactory().createLocalDriver(
                new LocalDriverConfiguration((TestConfiguration.browser)));

    }
}
