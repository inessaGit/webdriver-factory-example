package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testcases.LoadPropertiesTest;

@RunWith(Suite.class)
@Suite.SuiteClasses(value = {
        LoadPropertiesTest.class,
})

public class frameworkSuite {
}
