package testcases;

import jnhamilton.selenium.setup.LoadProperties;
import org.junit.Test;

import java.io.IOException;

public class LoadPropertiesTest {

    @Test
    public void canILoadTheUserProperties() throws IOException {
        String remote = LoadProperties.getPropertyValue("test.properties","Remote");
        System.out.println("The property loaded was Remote with value: " + remote );

        String browser = LoadProperties.getPropertyValue("test.properties","Browser");
        System.out.println("The property loaded was Browser with value: " + browser);

        String platform = LoadProperties.getPropertyValue("test.properties","Platform");
        System.out.println("The property loaded was Platform with value: " + platform);

        String browserVersion = LoadProperties.getPropertyValue("test.properties","BrowserVersion");
        System.out.println("The property loaded was BrowserVersion with value: " + browserVersion);

        String seleniumHubUrl = LoadProperties.getPropertyValue("test.properties","SeleniumHubUrl");
        System.out.println("The property loaded was SeleniumHubUrl with value: " + seleniumHubUrl);

        String seleniumHubPort = LoadProperties.getPropertyValue("test.properties","SeleniumHubPort");
        System.out.println("The property loaded was SeleniumHubPort with value: " + seleniumHubPort);

        String applicationUrl = LoadProperties.getPropertyValue("test.properties","ApplicationUrl");
        System.out.println("The property loaded was applicationUrl with value: " + applicationUrl);
    }
}
