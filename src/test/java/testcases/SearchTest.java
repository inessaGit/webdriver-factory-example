package testcases;

import jnhamilton.selenium.setup.TestConfiguration;
import jnhamilton.selenium.setup.TestDriverFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pageobjects.SearchPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SearchTest {

    public WebDriver driver;

    @Before
    public void setUp() {
        driver = new TestDriverFactory().createDriver();
        driver.navigate().to(TestConfiguration.applicationUrl);
    }

    @Test
    public void enterSearchTermAndVerifyResults(){
         SearchPage searchPage = new SearchPage(driver);
         searchPage.enterSearchTermAndSubmitSearch("mug cakes");
         assertThat(driver.getCurrentUrl(), containsString("mug+cakes"));
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
