package pageobjects;

import org.openqa.selenium.*;


import static jnhamilton.selenium.setup.TestConfiguration.applicationUrl;

public class SearchPage {

    private WebDriver driver;

    public SearchPage(WebDriver driver){
        this.driver = driver;
        if(!driver.getCurrentUrl().contains(applicationUrl)){
            throw new WebDriverException("We are not on the Google Search Page");
        }
    }

    final By SEARCH_TEXT_FIELD = By.id("lst-ib");

    public void enterSearchTermAndSubmitSearch(String text){
        WebElement searchButton = driver.findElement(SEARCH_TEXT_FIELD);
        searchButton.sendKeys(text);
        searchButton.sendKeys(Keys.ENTER);
    }
 }
