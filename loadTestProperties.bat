@ECHO OFF

REM - pass in the relevant parameters on the command line/Jenkins to set the file properties as desired
REM - need to be in the exact order below 
REM - for example: loadTestProperties.bat webdriver firefox Windows 32 http://localhost/ 4444 https://www.google.co.uk/
(
  echo Remote=%1
  echo Browser=%2
  echo Platform=%3
  echo BrowserVersion=%4
  echo SeleniumHubUrl=%5
  echo SeleniumHubPort=%6
  echo ApplicationUrl=%7
) > ./src/main/resources/test.properties
